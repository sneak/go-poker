package pokercore

type Suit rune

func (s Suit) String() string {
	return string(s)
}

func (s Suit) Symbol() string {
	// this is just to match Rank's Symbol() method
	return string(s)
}

const (
	CLUB    Suit = '\u2663' // ♣
	SPADE   Suit = '\u2660' // ♠
	DIAMOND Suit = '\u2666' // ♦
	HEART   Suit = '\u2665' // ♥
)
