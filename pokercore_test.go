package pokercore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type ShuffleTestResults []struct {
	SeedVal  int64
	Expected string
}

func TestPokerDeck(t *testing.T) {
	d := NewDeck()
	//fmt.Printf("newdeck: %+v\n", d)
	d.ShuffleDeterministically(437)
	//fmt.Printf("deterministically shuffled deck: %+v\n", d)
	cards := d.Deal(7)
	expected := "6♥,A♦,7♥,9♣,6♠,9♥,8♣"
	//fmt.Printf("deterministically shuffled deck after dealing: %+v\n", d)
	//fmt.Printf("cards: %+v\n", cards)
	assert.Equal(t, expected, cards.String())

	x := d.Count()
	assert.Equal(t, 45, x)

	d.ShuffleDeterministically(123456789)
	cards = d.Deal(10)
	expected = "A♣,7♠,8♠,4♠,7♦,K♠,2♣,J♦,A♠,2♦"
	assert.Equal(t, expected, cards.String())
	x = d.Count()
	assert.Equal(t, 35, x)

}

func TestDealing(t *testing.T) {
	d := NewDeckFromCards(Cards{
		Card{Rank: ACE, Suit: HEART},
		Card{Rank: DEUCE, Suit: HEART},
		Card{Rank: THREE, Suit: HEART},
		Card{Rank: FOUR, Suit: HEART},
		Card{Rank: FIVE, Suit: HEART},
	})
	cards := d.Deal(5)
	expected := "A♥,2♥,3♥,4♥,5♥"
	assert.Equal(t, expected, cards.String())
	x := d.Count()
	assert.Equal(t, 0, x)
}

func TestSpecialCaseOfFiveHighStraightFlush(t *testing.T) {
	// actual bug from first implementation
	d := NewDeckFromCards(Cards{
		Card{Rank: ACE, Suit: HEART},
		Card{Rank: DEUCE, Suit: HEART},
		Card{Rank: THREE, Suit: HEART},
		Card{Rank: FOUR, Suit: HEART},
		Card{Rank: FIVE, Suit: HEART},
	})
	cards := d.Deal(5)
	expected := "A♥,2♥,3♥,4♥,5♥"
	assert.Equal(t, expected, cards.String())
	ph, err := cards.PokerHand()
	assert.Nil(t, err)
	description := ph.Description()
	assert.Equal(t, "a five high straight flush in ♥", description)
}

func TestSpecialCaseOfFiveHighStraight(t *testing.T) {
	// actual bug from first implementation
	d := NewDeckFromCards(Cards{
		Card{Rank: ACE, Suit: HEART},
		Card{Rank: DEUCE, Suit: HEART},
		Card{Rank: THREE, Suit: SPADE},
		Card{Rank: FOUR, Suit: HEART},
		Card{Rank: FIVE, Suit: CLUB},
	})
	d.ShuffleDeterministically(123456789)
	cards := d.Deal(5)

	cards = cards.SortByRankAscending()
	expected := "2♥,3♠,4♥,5♣,A♥"
	assert.Equal(t, expected, cards.String())
	ph, err := cards.PokerHand()
	assert.Nil(t, err)
	description := ph.Description()
	assert.Equal(t, "a five high straight", description)
}
