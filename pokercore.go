package pokercore

import (
	"encoding/binary"

	crand "crypto/rand"

	log "github.com/sirupsen/logrus"
)

type TestGenerationIteration struct {
	Deck *Deck
	Seed int64
}

func cryptoUint64() (v uint64) {
	err := binary.Read(crand.Reader, binary.BigEndian, &v)
	if err != nil {
		log.Fatal(err)
	}
	log.Debugf("crand cryptosource is returning Uint64: %d", v)
	return v
}
