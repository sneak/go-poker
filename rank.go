package pokercore

import "fmt"

type Rank rune

const (
	ACE   Rank = 'A'
	DEUCE Rank = '2'
	THREE Rank = '3'
	FOUR  Rank = '4'
	FIVE  Rank = '5'
	SIX   Rank = '6'
	SEVEN Rank = '7'
	EIGHT Rank = '8'
	NINE  Rank = '9'
	TEN   Rank = 'T'
	JACK  Rank = 'J'
	QUEEN Rank = 'Q'
	KING  Rank = 'K'
)

func (r Rank) String() string {
	switch r {
	case ACE:
		return "ace"
	case DEUCE:
		return "deuce"
	case THREE:
		return "three"
	case FOUR:
		return "four"
	case FIVE:
		return "five"
	case SIX:
		return "six"
	case SEVEN:
		return "seven"
	case EIGHT:
		return "eight"
	case NINE:
		return "nine"
	case TEN:
		return "ten"
	case JACK:
		return "jack"
	case QUEEN:
		return "queen"
	case KING:
		return "king"
	}
	return ""
}

func (r Rank) Symbol() string {
	return string(r)
}

func (r Rank) Int() int {
	return int(r.Score())
}

func (r Rank) HandScore() HandScore {
	return HandScore(r.Int())
}

func (r Rank) Article() string {
	switch r {
	case ACE:
		return "an"
	case EIGHT:
		return "an"
	default:
		return "a"
	}
}

func (r Rank) WithArticle() string {
	return fmt.Sprintf("%s %s", r.Article(), r)
}

func (r Rank) Pluralize() string {
	switch r {
	case ACE:
		return "aces"
	case DEUCE:
		return "deuces"
	case THREE:
		return "threes"
	case FOUR:
		return "fours"
	case FIVE:
		return "fives"
	case SIX:
		return "sixes"
	case SEVEN:
		return "sevens"
	case EIGHT:
		return "eights"
	case NINE:
		return "nines"
	case TEN:
		return "tens"
	case JACK:
		return "jacks"
	case QUEEN:
		return "queens"
	case KING:
		return "kings"
	default:
		panic("nope")
	}
}

func (r Rank) Score() HandScore {
	switch r {
	case DEUCE:
		return 2
	case THREE:
		return 3
	case FOUR:
		return 4
	case FIVE:
		return 5
	case SIX:
		return 6
	case SEVEN:
		return 7
	case EIGHT:
		return 8
	case NINE:
		return 9
	case TEN:
		return 10
	case JACK:
		return 11
	case QUEEN:
		return 12
	case KING:
		return 13
	case ACE:
		return 14
	}
	return 0
}
