.PHONY: examples

default: run

run: simgame

simgame: examples
	RANDOM_SHUFFLE=1 ./bin/simgame

clean:
	rm -rf bin *.test

test:
	go test -count=1 .

examples:
	test -d bin || mkdir bin
	go build -o bin/ ./examples/...

lint:
	golangci-lint run

fmt:
	go fmt ./...
