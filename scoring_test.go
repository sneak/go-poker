package pokercore

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandDescripionBug(t *testing.T) {
	playerCount := 8
	d := NewDeck()
	d.ShuffleDeterministically(1337)
	players := make([]*Cards, playerCount)
	for i := 1; i-1 < playerCount; i++ {
		c := d.Deal(2)
		players[i-1] = &c
		t.Logf("Player %d dealt: %+v\n", i, c)
	}
	t.Logf("Players: %+v\n", players)

	community := d.Deal(5)
	t.Logf("Community: %+v\n", community)

	var playerResults []*PokerHand

	for i := 1; i-1 < playerCount; i++ {
		t.Logf("Player %d hole cards: %+v\n", i, *players[i-1])
		pc := append(*players[i-1], community...)
		t.Logf("Player %d cards available: %+v\n", i, pc)
		hand, err := pc.IdentifyBestFiveCardPokerHand()
		assert.Nil(t, err, "Expected no error")
		ph, err := hand.PokerHand()
		assert.Nil(t, err, "Expected no error")
		t.Logf("Player %d five cards used: %+v\n", i, hand)
		t.Logf("Player %d poker hand: %+v\n", i, ph)
		t.Logf("Player %d best hand description: %s\n", i, ph.Description())
		playerResults = append(playerResults, ph)
	}

	weirdOne := playerResults[7]

	t.Logf("Weird one: %v\n", weirdOne)
	t.Logf("Weird one description: %s\n", weirdOne.Description())

	// T♠,7♠,9♦,7♣,T♥
	assert.Equal(t, weirdOne.Description(), "two pair, tens and sevens with a nine")

	scoreShouldBe := ScoreTwoPair
	scoreShouldBe += 10000 * TEN.Score()
	scoreShouldBe += 100 * SEVEN.Score()
	scoreShouldBe += NINE.Score()
	assert.Equal(t, weirdOne.Score, scoreShouldBe)

	cards := weirdOne.Hand

	assert.True(t, cards.containsTwoPair(), "Expected hand to be two pair")

	bp := cards.twoPairBiggestPair() // returns Rank, because describing a pair
	assert.Equal(t, bp, TEN, "Expected biggest pair to be a ten")

	sp := cards.twoPairSmallestPair() // returns Rank, because describing a pair
	assert.Equal(t, sp, SEVEN, "Expected smallest pair to be a seven")

	k := cards.twoPairKicker() // returns Card, because describing a single card
	assert.Equal(t, k.Rank, NINE, "Expected kicker to be a nine")

}

func TestAceLowStraight(t *testing.T) {
	hand := Cards{
		AceOfSpades(),
		DeuceOfHearts(),
		ThreeOfDiamonds(),
		FourOfClubs(),
		FiveOfSpades(),
	}
	assert.True(t, hand.containsStraight(), "Expected hand to be a straight")
	ph, err := hand.PokerHand()
	assert.Nil(t, err, "Expected no error")
	assert.Greater(t, ph.Score, 0, "Expected score to be greater than 0")
	assert.Less(t, ph.Score, 100000000000000000, "Expected score to be less than 100000000000000000")
	assert.Equal(t, ph.Score, ScoreStraight+FIVE.Score())
	assert.Equal(t, ph.Description(), "a five high straight")
	assert.True(t, hand.HighestRank() == ACE, "Expected highest rank to be an ace")
	s := hand.SortByRankAscending()
	assert.True(t, s.First().Rank == DEUCE, "Expected first card to be a deuce")
	assert.True(t, s.Last().Rank == ACE, "Expected last card in sorted to be a ace")
	assert.True(t, s.Second().Rank == THREE, "Expected second card to be a three")
	assert.True(t, s.Third().Rank == FOUR, "Expected third card to be a four")
	assert.True(t, s.Fourth().Rank == FIVE, "Expected fourth card to be a five")
	assert.True(t, s.Fifth().Rank == ACE, "Expected fifth card to be an ace")
}

func TestAceHighStraight(t *testing.T) {
	hand := Cards{
		TenOfSpades(),
		JackOfHearts(),
		KingOfClubs(),
		AceOfSpades(),
		QueenOfDiamonds(),
	}

	assert.True(t, hand.containsStraight(), "Expected hand to be a straight")
	newDeck := NewDeckFromCards(hand)
	newDeck.ShuffleDeterministically(123456789)
	shuffledHand := newDeck.Deal(5)
	assert.True(t, shuffledHand.containsStraight(), "Expected hand to still be a straight after shuffle")
	assert.True(t, shuffledHand.HighestRank() == ACE, "Expected highest rank to be an ace")
	sortedHand := shuffledHand.SortByRankAscending()
	assert.True(t, sortedHand[0].Rank == TEN, "Expected lowest rank to be a ten")
	assert.True(t, sortedHand[1].Rank == JACK, "Expected second lowest rank to be a jack")
	assert.True(t, sortedHand[2].Rank == QUEEN, "Expected third lowest rank to be a queen")
	assert.True(t, sortedHand[3].Rank == KING, "Expected fourth lowest rank to be a king")
	assert.True(t, sortedHand[4].Rank == ACE, "Expected highest rank to be an ace")
}

func TestOtherStraight(t *testing.T) {
	hand := Cards{
		DeuceOfSpades(),
		ThreeOfHearts(),
		FourOfDiamonds(),
		FiveOfClubs(),
		SixOfSpades(),
	}
	assert.True(t, hand.containsStraight(), "Expected hand to be a straight")

	newDeck := NewDeckFromCards(hand)
	newDeck.ShuffleDeterministically(123456789)
	//fmt.Printf("Shuffled deck: %s\n", newDeck.String())
	//fmt.Printf("new deck has %d cards\n", newDeck.Count())
	shuffledHand := newDeck.Deal(5)
	assert.True(t, shuffledHand.containsStraight(), "Expected hand to still be a straight after shuffle")
	assert.False(t, shuffledHand.containsTwoPair(), "Expected hand to not be two pair")
	assert.False(t, shuffledHand.containsPair(), "Expected hand to not be a pair")
	assert.True(t, shuffledHand.HighestRank() == SIX, "Expected highest rank to be a six")
	assert.True(t, shuffledHand.SortByRankAscending().First().Rank == DEUCE, "Expected first card to be a deuce")
}

func TestFlush(t *testing.T) {
	hand := Cards{
		AceOfSpades(),
		DeuceOfSpades(),
		ThreeOfSpades(),
		FourOfSpades(),
		SixOfSpades(),
	}
	assert.True(t, hand.containsFlush(), "Expected hand to be a flush")
	newDeck := NewDeckFromCards(hand)
	newDeck.ShuffleDeterministically(123456789)
	//fmt.Printf("Shuffled deck: %s\n", newDeck.String())
	//fmt.Printf("new deck has %d cards\n", newDeck.Count())
	shuffledHand := newDeck.Deal(5)
	assert.True(t, shuffledHand.containsFlush(), "Expected hand to still be a flush after shuffle")

	// flush value is the sum of the ranks, just like high card
	x := ScoreFlush
	x += ACE.Score()
	x += DEUCE.Score()
	x += THREE.Score()
	x += FOUR.Score()
	x += SIX.Score()
	//fmt.Printf("a-2-3-4-6 flush score should be: %d\n", x)

	ph, err := shuffledHand.PokerHand()
	assert.Nil(t, err, "Expected no error")
	assert.Greater(t, ph.Score, 0, "Expected score to be nonzero 0")
	assert.Equal(t, ph.Score, x)
}

func TestStraightFlush(t *testing.T) {
	hand := Cards{
		SixOfSpades(),
		DeuceOfSpades(),
		ThreeOfSpades(),
		FourOfSpades(),
		FiveOfSpades(),
	}
	assert.True(t, hand.containsStraight(), "Expected hand to be a straight")
	assert.True(t, hand.containsFlush(), "Expected hand to be a flush")
	assert.True(t, hand.containsStraightFlush(), "Expected hand to be a straight flush")
	assert.False(t, hand.containsRoyalFlush(), "Expected hand to not be a royal flush")
	assert.False(t, hand.containsFourOfAKind(), "Expected hand to not be four of a kind")
	assert.False(t, hand.containsFullHouse(), "Expected hand to not be a full house")
	assert.False(t, hand.containsThreeOfAKind(), "Expected hand to not be three of a kind")
	assert.False(t, hand.containsTwoPair(), "Expected hand to not be two pair")
	assert.False(t, hand.containsPair(), "Expected hand to not be a pair")

	assert.True(t, hand.HighestRank() == SIX, "Expected highest rank to be a six")

	nd := NewDeckFromCards(hand)
	nd.ShuffleDeterministically(123456789)
	//fmt.Printf("Shuffled deck: %s\n", nd.String())
	//fmt.Printf("new deck has %d cards\n", nd.Count())
	shuffledHand := nd.Deal(5)
	assert.True(t, shuffledHand.containsStraightFlush(), "Expected hand to still be a straight flush after shuffle")
	assert.True(t, shuffledHand.HighestRank() == SIX, "Expected highest rank to still be a six after shuffle")
	assert.True(t, shuffledHand.HighestRank() == SIX, "Expected highest rank to be a six after shuffle even with aces low")
}

func TestFourOfAKind(t *testing.T) {
	hand := Cards{
		SixOfSpades(),
		SixOfHearts(),
		SixOfDiamonds(),
		SixOfClubs(),
		FiveOfSpades(),
	}
	assert.False(t, hand.containsStraight(), "Expected hand to not be a straight")
	assert.False(t, hand.containsFlush(), "Expected hand to not be a flush")
	assert.False(t, hand.containsStraightFlush(), "Expected hand to not be a straight flush")
	assert.False(t, hand.containsRoyalFlush(), "Expected hand to not be a royal flush")
	assert.True(t, hand.containsFourOfAKind(), "Expected hand to be four of a kind")
	assert.False(t, hand.containsFullHouse(), "Expected hand to not be a full house")

	// note that these are *expected* to be true.  the contains* functions
	// are used in the PokerHand.calculateScore method to determine the best hand
	// and are checked in sequence of descending value, so if a hand is four of a kind
	// it will not be checked for full house, three of a kind, etc.

	// technically quads *is* two pair also, and a hand with quads does
	// indeed contain three of a kind, and contains a pair.
	assert.True(t, hand.containsThreeOfAKind(), "Expected hand to contain three of a kind")
	assert.True(t, hand.containsTwoPair(), "Expected hand to contain two pair")
	assert.True(t, hand.containsPair(), "Expected hand to contain a pair")

	assert.True(t, hand.HighestRank() == SIX, "Expected highest rank to be a six")
}

func TestRoyalFlush(t *testing.T) {
	hand := Cards{
		TenOfSpades(),
		JackOfSpades(),
		QueenOfSpades(),
		KingOfSpades(),
		AceOfSpades(),
	}
	assert.True(t, hand.containsStraight(), "Expected hand to be a straight")
	assert.True(t, hand.containsFlush(), "Expected hand to be a flush")
	assert.True(t, hand.containsStraightFlush(), "Expected hand to be a straight flush")
	assert.True(t, hand.containsRoyalFlush(), "Expected hand to be a royal flush")

	assert.False(t, hand.containsFourOfAKind(), "Expected hand to not be four of a kind")
	assert.False(t, hand.containsFullHouse(), "Expected hand to not be a full house")
	assert.False(t, hand.containsThreeOfAKind(), "Expected hand to not be three of a kind")
	assert.False(t, hand.containsTwoPair(), "Expected hand to not be two pair")
	assert.False(t, hand.containsPair(), "Expected hand to not be a pair")

	assert.True(t, hand.HighestRank() == ACE, "Expected highest rank to be an ace")
	assert.False(t, hand.HighestRank() == TEN, "Expected highest rank to not be an ace")
}

func TestUnmadeHand(t *testing.T) {
	hand := Cards{
		TenOfSpades(),
		JackOfDiamonds(),
		QueenOfSpades(),
		KingOfSpades(),
		DeuceOfSpades(),
	}
	assert.False(t, hand.containsStraight(), "Expected hand to not be a straight")
	assert.False(t, hand.containsFlush(), "Expected hand to not be a flush")
	assert.False(t, hand.containsStraightFlush(), "Expected hand to not be a straight flush")
	assert.False(t, hand.containsRoyalFlush(), "Expected hand to not be a royal flush")
	assert.False(t, hand.containsFourOfAKind(), "Expected hand to not be four of a kind")
	assert.False(t, hand.containsFullHouse(), "Expected hand to not be a full house")
	assert.False(t, hand.containsThreeOfAKind(), "Expected hand to not be three of a kind")
	assert.False(t, hand.containsTwoPair(), "Expected hand to not be two pair")
	assert.False(t, hand.containsPair(), "Expected hand to not be a pair")
	assert.True(t, hand.HighestRank() == KING, "Expected highest rank to be a king")
	assert.True(t, hand.isUnmadeHand(), "Expected hand to be unmade")
}

func TestTwoPair(t *testing.T) {
	hand := Cards{
		KingOfSpades(),
		JackOfDiamonds(),
		JackOfSpades(),
		KingOfDiamonds(),
		TenOfSpades(),
	}
	assert.False(t, hand.containsStraight(), "Expected hand to not be a straight")
	assert.False(t, hand.containsFlush(), "Expected hand to not be a flush")
	assert.False(t, hand.containsStraightFlush(), "Expected hand to not be a straight flush")
	assert.False(t, hand.containsRoyalFlush(), "Expected hand to not be a royal flush")
	assert.False(t, hand.containsFourOfAKind(), "Expected hand to not be four of a kind")
	assert.False(t, hand.containsFullHouse(), "Expected hand to not be a full house")
	assert.False(t, hand.containsThreeOfAKind(), "Expected hand to not be three of a kind")
	assert.True(t, hand.containsTwoPair(), "Expected hand to be two pair")
	assert.True(t, hand.containsPair(), "Expected hand to also be a pair")
	assert.True(t, hand.HighestRank() == KING, "Expected highest rank to be a king")
	assert.False(t, hand.isUnmadeHand(), "Expected hand to not be unmade")
}

func TestDetectDuplicates(t *testing.T) {
	hand := Cards{
		KingOfSpades(),
		JackOfDiamonds(),
		JackOfSpades(),
		KingOfSpades(),
		TenOfSpades(),
	}
	assert.True(t, hand.containsDuplicates(), "Expected hand to contain duplicates")
}

func TestHandScore(t *testing.T) {
	hand := Cards{
		KingOfSpades(),
		JackOfDiamonds(),
		JackOfSpades(),
		KingOfDiamonds(),
		TenOfSpades(),
	}

	ph, error := hand.PokerHand()
	assert.Nil(t, error, "Expected no error")
	assert.True(t, ph.Score > 0, "Expected score to be nonzero 0")
	assert.True(t, ph.Score < 100000000000000000, "Expected score to be less than 100000000000000000")

	//fmt.Printf("PokerHand: %v+\n", ph)
	//fmt.Printf("PH score: %d\n", ph.Score)
}

func TestTwoPairBug(t *testing.T) {
	// this is an actual bug in the first implementation
	c, err := NewCardsFromString("9♠,9♣,Q♥,Q♦,K♣")
	assert.Nil(t, err, "Expected no error")

	//fmt.Printf("Cards: %v+\n", c)

	ph, err := c.PokerHand()
	assert.Nil(t, err, "Expected no error")
	assert.Greater(t, ph.Score, 0, "Expected score to be nonzero 0")
	desc := ph.Description()
	assert.Equal(t, desc, "two pair, queens and nines with a king")

	//fmt.Printf("PokerHand: %v+\n", ph)
	//fmt.Printf("PH score: %d\n", ph.Score)
}

func TestScoringStructureQuads(t *testing.T) {
	handA := Cards{
		DeuceOfSpades(),
		DeuceOfHearts(),
		DeuceOfDiamonds(),
		DeuceOfClubs(),
		AceOfSpades(),
	}

	handB := Cards{
		ThreeOfSpades(),
		ThreeOfHearts(),
		ThreeOfDiamonds(),
		ThreeOfClubs(),
		DeuceOfSpades(),
	}

	phA, err := handA.PokerHand()
	assert.Nil(t, err, "Expected no error")
	phB, err := handB.PokerHand()
	assert.Nil(t, err, "Expected no error")
	assert.Greater(t, phB.Score, phA.Score, "Expected hand B to be higher than hand A")
}

func TestScoringStructureFullHouse(t *testing.T) {
	handA := Cards{
		DeuceOfSpades(),
		DeuceOfHearts(),
		DeuceOfDiamonds(),
		AceOfSpades(),
		AceOfHearts(),
	}

	phA, err := handA.PokerHand()
	assert.Nil(t, err, "Expected no error")
	deucesFullOfAcesScore := phA.Score

	handB := Cards{
		ThreeOfSpades(),
		ThreeOfHearts(),
		ThreeOfDiamonds(),
		DeuceOfSpades(),
		DeuceOfHearts(),
	}
	phB, err := handB.PokerHand()
	assert.Nil(t, err, "Expected no error")
	threesFullOfDeucesScore := phB.Score

	assert.Greater(t, threesFullOfDeucesScore, deucesFullOfAcesScore, "Expected Threes full of deuces to beat deuces full of aces")
}
