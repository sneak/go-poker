package pokercore

import (
	"context"
	"testing"
	"time"

	"git.eeqj.de/sneak/timingbench"
	"github.com/stretchr/testify/assert"
)

func TestShuffleSpeed(t *testing.T) {
	iterations := 1000
	t.Logf("Running %d iterations of shuffle speed test", iterations)
	// Create a context with a timeout for cancellation.
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	// Measure the execution times of the sample function.
	d := NewDeck()
	result, err := timingbench.TimeFunction(ctx, func() { d.ShuffleRandomly() }, iterations)
	if err != nil {
		t.Fatalf("Error measuring function: %v", err)
	}
	// Print the timing results.
	t.Logf(result.String())
}

func TestHandFindingSpeedFiveCard(t *testing.T) {
	iterations := 1000
	t.Logf("Running %d iterations of hand finding speed test for 5 card hand", iterations)
	measureHandFinding(t, iterations, 5)
}

func TestHandFindingSpeedSevenCard(t *testing.T) {
	iterations := 1000
	t.Logf("Running %d iterations of hand finding speed test for 7 card hand", iterations)
	measureHandFinding(t, iterations, 7)
}

func TestHandFindingSpeedNineCard(t *testing.T) {
	iterations := 100
	t.Logf("Running %d iterations of hand finding speed test for 9 card hand", iterations)
	measureHandFinding(t, iterations, 9)
}

func measureHandFinding(t *testing.T, iterations int, cardCount int) {
	// Create a context with a timeout for cancellation.
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	// Measure the execution times of the sample function.
	result, err := timingbench.TimeFunction(ctx, func() {
		findHandInRandomCards(t, int(123456789), cardCount) // check for hand in 10 cards
	}, iterations)
	if err != nil {
		t.Fatalf("Error measuring function: %v", err)
	}
	t.Logf("Searching %d random cards for a hand takes on average %s", cardCount, result.Mean)
	t.Logf("Over %d iterations the min was %s and the max was %s", iterations, result.Min, result.Max)
	t.Logf("The standard deviation was %s", result.StdDev)
	t.Logf("The median was %s", result.Median)

	// Print the timing results.
	//t.Logf(result.String())
}

func findHandInRandomCards(t *testing.T, shuffleSeed int, cardCount int) {
	d := NewDeck()
	d.ShuffleDeterministically(int64(shuffleSeed))
	cards := d.Deal(cardCount)
	hand, err := cards.IdentifyBestFiveCardPokerHand()
	assert.Nil(t, err, "Expected no error")
	ph, err := hand.PokerHand()
	assert.Nil(t, err, "Expected no error")
	//assert.Greater(t, ph.Score, 0, "Expected score to be nonzero 0")
	desc := ph.Description()
	assert.NotEmpty(t, desc, "Expected description to not be empty")
}
