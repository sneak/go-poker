package pokercore

func AceOfSpades() Card   { return Card{ACE, SPADE} }
func DeuceOfSpades() Card { return Card{DEUCE, SPADE} }
func ThreeOfSpades() Card { return Card{THREE, SPADE} }
func FourOfSpades() Card  { return Card{FOUR, SPADE} }
func FiveOfSpades() Card  { return Card{FIVE, SPADE} }
func SixOfSpades() Card   { return Card{SIX, SPADE} }
func SevenOfSpades() Card { return Card{SEVEN, SPADE} }
func EightOfSpades() Card { return Card{EIGHT, SPADE} }
func NineOfSpades() Card  { return Card{NINE, SPADE} }
func TenOfSpades() Card   { return Card{TEN, SPADE} }
func JackOfSpades() Card  { return Card{JACK, SPADE} }
func QueenOfSpades() Card { return Card{QUEEN, SPADE} }
func KingOfSpades() Card  { return Card{KING, SPADE} }

func AceOfHearts() Card   { return Card{ACE, HEART} }
func DeuceOfHearts() Card { return Card{DEUCE, HEART} }
func ThreeOfHearts() Card { return Card{THREE, HEART} }
func FourOfHearts() Card  { return Card{FOUR, HEART} }
func FiveOfHearts() Card  { return Card{FIVE, HEART} }
func SixOfHearts() Card   { return Card{SIX, HEART} }
func SevenOfHearts() Card { return Card{SEVEN, HEART} }
func EightOfHearts() Card { return Card{EIGHT, HEART} }
func NineOfHearts() Card  { return Card{NINE, HEART} }
func TenOfHearts() Card   { return Card{TEN, HEART} }
func JackOfHearts() Card  { return Card{JACK, HEART} }
func QueenOfHearts() Card { return Card{QUEEN, HEART} }
func KingOfHearts() Card  { return Card{KING, HEART} }

func AceOfDiamonds() Card   { return Card{ACE, DIAMOND} }
func DeuceOfDiamonds() Card { return Card{DEUCE, DIAMOND} }
func ThreeOfDiamonds() Card { return Card{THREE, DIAMOND} }
func FourOfDiamonds() Card  { return Card{FOUR, DIAMOND} }
func FiveOfDiamonds() Card  { return Card{FIVE, DIAMOND} }
func SixOfDiamonds() Card   { return Card{SIX, DIAMOND} }
func SevenOfDiamonds() Card { return Card{SEVEN, DIAMOND} }
func EightOfDiamonds() Card { return Card{EIGHT, DIAMOND} }
func NineOfDiamonds() Card  { return Card{NINE, DIAMOND} }
func TenOfDiamonds() Card   { return Card{TEN, DIAMOND} }
func JackOfDiamonds() Card  { return Card{JACK, DIAMOND} }
func QueenOfDiamonds() Card { return Card{QUEEN, DIAMOND} }
func KingOfDiamonds() Card  { return Card{KING, DIAMOND} }

func AceOfClubs() Card   { return Card{ACE, CLUB} }
func DeuceOfClubs() Card { return Card{DEUCE, CLUB} }
func ThreeOfClubs() Card { return Card{THREE, CLUB} }
func FourOfClubs() Card  { return Card{FOUR, CLUB} }
func FiveOfClubs() Card  { return Card{FIVE, CLUB} }
func SixOfClubs() Card   { return Card{SIX, CLUB} }
func SevenOfClubs() Card { return Card{SEVEN, CLUB} }
func EightOfClubs() Card { return Card{EIGHT, CLUB} }
func NineOfClubs() Card  { return Card{NINE, CLUB} }
func TenOfClubs() Card   { return Card{TEN, CLUB} }
func JackOfClubs() Card  { return Card{JACK, CLUB} }
func QueenOfClubs() Card { return Card{QUEEN, CLUB} }
func KingOfClubs() Card  { return Card{KING, CLUB} }
