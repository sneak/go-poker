package pokercore

import (
	"errors"
	"fmt"
)

type PokerHandType int

const (
	HighCard PokerHandType = iota
	Pair
	TwoPair
	ThreeOfAKind
	Straight
	Flush
	FullHouse
	FourOfAKind
	StraightFlush
	RoyalFlush
)

type PokerHand struct {
	Hand  Cards
	Type  PokerHandType
	Score HandScore
}

func (c Cards) Append(other Cards) Cards {
	return append(c, other...)
}

func (c Cards) PokerHand() (*PokerHand, error) {
	if c.containsDuplicates() {
		return nil, errors.New("hand must have no duplicates to be scored as a poker hand")
	}

	ph := new(PokerHand)

	// IdentifyBestFiveCardPokerHand() calls us to score hands
	// but only if the hand is 5 cards exactly which avoids recursion loop
	if len(c) > 5 {
		phc, err := c.IdentifyBestFiveCardPokerHand()
		if err != nil {
			// this should in theory never happen
			return nil, err
		}
		ph.Hand = phc.SortByRankAscending()
	} else if len(c) == 5 {
		ph.Hand = c.SortByRankAscending()
	} else {
		return nil, errors.New("hand must have at least 5 cards to be scored as a poker hand")
	}

	// this doesn't return an error because we've already checked for
	// duplicates and we have already identified the best 5-card hand

	// this code used to be here, but got factored out so it can go in
	// scoring.go
	ph.calculateScore()

	return ph, nil
}

func (ph PokerHand) ToSortedCards() Cards {
	// I believe ph.Hand is already sorted, but in any case it's only 5
	// cards and sorting it costs ~nothing
	return ph.Hand.SortByRankAscending()
}

func (ph PokerHand) Compare(other PokerHand) int {
	if ph.Score > other.Score {
		return 1
	}
	if ph.Score < other.Score {
		return -1
	}
	return 0
}

func (ph PokerHand) HighestRank() Rank {
	return ph.Hand.HighestRank()
}

func (ph PokerHand) String() string {
	return fmt.Sprintf("<PokerHand: %s (%s)>", ph.Hand.String(), ph.Description())
}

func (c PokerHand) Description() string {
	if c.Type == RoyalFlush {
		return fmt.Sprintf("a royal flush in %s", c.Hand[0].Suit)
	}
	if c.Hand.containsStraightFlush() {
		if c.Hand[3].Rank == FIVE && c.Hand[4].Rank == ACE {
			// special case for steel wheel
			return fmt.Sprintf("%s high straight flush in %s", c.Hand[3].Rank.WithArticle(), c.Hand[4].Suit)
		}
		return fmt.Sprintf("%s high straight flush in %s", c.HighestRank().WithArticle(), c.Hand[4].Suit)
	}
	if c.Hand.containsFourOfAKind() {
		return fmt.Sprintf("four %s with %s", c.Hand.fourOfAKindRank().Pluralize(), c.Hand.fourOfAKindKicker().Rank.WithArticle())
	}
	if c.Hand.containsFullHouse() {
		return fmt.Sprintf("a full house, %s full of %s", c.Hand.fullHouseTripsRank().Pluralize(), c.Hand.fullHousePairRank().Pluralize())
	}
	if c.Hand.containsFlush() {
		return fmt.Sprintf("%s high flush in %s", c.HighestRank().WithArticle(), c.Hand[4].Suit)
	}
	if c.Hand.containsStraight() {
		if c.Hand[3].Rank == FIVE && c.Hand[4].Rank == ACE {
			// special case for wheel straight
			return fmt.Sprintf("%s high straight", c.Hand[3].Rank.WithArticle())
		}
		return fmt.Sprintf("%s high straight", c.HighestRank().WithArticle())
	}
	if c.Hand.containsThreeOfAKind() {
		return fmt.Sprintf(
			"three %s with %s and %s",
			c.Hand.threeOfAKindTripsRank().Pluralize(),
			c.Hand.threeOfAKindFirstKicker().Rank.WithArticle(),
			c.Hand.threeOfAKindSecondKicker().Rank.WithArticle(),
		)
	}
	if c.Hand.containsTwoPair() {
		return fmt.Sprintf(
			"two pair, %s and %s with %s",
			c.Hand.twoPairBiggestPair().Pluralize(),
			c.Hand.twoPairSmallestPair().Pluralize(),
			c.Hand.twoPairKicker().Rank.WithArticle(),
		)
	}
	if c.Hand.containsPair() {
		return fmt.Sprintf(
			"a pair of %s with %s, %s, and %s",
			c.Hand.pairRank().Pluralize(),
			c.Hand.pairFirstKicker().Rank.WithArticle(),
			c.Hand.pairSecondKicker().Rank.WithArticle(),
			c.Hand.pairThirdKicker().Rank.WithArticle(),
		)
	}

	return fmt.Sprintf(
		// "ace high with an eight, a seven, a six, and a deuce"
		"%s high with %s, %s, %s, and %s",
		c.Hand[4].Rank,
		c.Hand[3].Rank.WithArticle(),
		c.Hand[2].Rank.WithArticle(),
		c.Hand[1].Rank.WithArticle(),
		c.Hand[0].Rank.WithArticle(),
	)
}
