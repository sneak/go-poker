module git.eeqj.de/sneak/pokercore

go 1.22.2

require (
	git.eeqj.de/sneak/timingbench v0.0.0-20240519025145-fb13c5c56a02
	github.com/logrusorgru/aurora/v4 v4.0.0
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475
	github.com/schollz/progressbar/v3 v3.14.2
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/term v0.20.0 // indirect
	gonum.org/v1/gonum v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
