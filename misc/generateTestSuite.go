package main

import (
	"fmt"

	"git.eeqj.de/sneak/pokercore"
)

func main() {
	myDeck := pokercore.NewDeck()
	myDeck.ShuffleRandomly()
	a := myDeck.Deal(5)
	b := myDeck.Deal(7)
	a.PrintToTerminal()
	fmt.Printf("\n%#v\n", a)
	b.PrintToTerminal()
	fmt.Printf("\n%#v\n", b)
}
