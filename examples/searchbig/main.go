package main

import (
	"fmt"

	"git.eeqj.de/sneak/pokercore"
)

func main() {

	for i := 0; i < 10; i++ {
		d := pokercore.NewDeck()
		fmt.Printf("deck before shuffling: %s\n", d.FormatForTerminal())
		d.ShuffleRandomly()
		fmt.Printf("deck after shuffling: %s\n", d.FormatForTerminal())
		offTheTop := d.Deal(11)
		fmt.Printf("off the top: %s\n", offTheTop.FormatForTerminal())
		wh, err := offTheTop.IdentifyBestFiveCardPokerHand()
		if err != nil {
			panic(err)
		}
		fmt.Printf("best hand: %s\n", wh.FormatForTerminalSorted(pokercore.AceHighAscending))
		score, err := wh.PokerHandScore()
		if err != nil {
			panic(err)
		}
		fmt.Printf("best hand score: %s\n", score)
		ph, err := wh.PokerHand()
		if err != nil {
			panic(err)
		}
		fmt.Printf("best hand string: %s\n", ph.Description())
	}
}
