package main

import (
	"fmt"
	"os"
	"time"

	"git.eeqj.de/sneak/pokercore"
)

// just like on tv
var delayTime = 2 * time.Second
var playerCount = 8

type Player struct {
	Hand       pokercore.Cards
	ScoredHand *pokercore.PokerHand
	Position   int
}

type Game struct {
	Deck      *pokercore.Deck
	Players   []*Player
	Community pokercore.Cards
	Street    int
}

func suspense() {
	fmt.Printf("########################################\n")
	if os.Getenv("NO_LAG") == "" {
		time.Sleep(delayTime)
	}
}

func NewGame() *Game {

	// this "randomly chosen" seed somehow deals pocket queens, pocket kings, and pocket aces to three players.
	// what are the odds? lol
	//g := NewGame(1337))

	// nothing up my sleeve:
	seed := 3141592653

	g := &Game{}
	g.Street = 0

	g.Deck = pokercore.NewDeck()
	g.SpreadCards()

	fmt.Printf("Shuffle up and deal!\n")
	suspense()
	fmt.Printf("Shuffling...\n")

	if os.Getenv("RANDOM_SHUFFLE") != "" {
		g.Deck.ShuffleRandomly()
	} else {
		fmt.Printf("Using deterministic shuffle seed: %d\n", seed)
		g.Deck.ShuffleDeterministically(int64(seed))
	}
	return g
}

func (g *Game) StreetAsString() string {
	switch g.Street {
	case 0:
		return "pre-flop"
	case 1:
		return "flop"
	case 2:
		return "turn"
	case 3:
		return "river"
	}
	return "unknown"
}

func (g *Game) SpreadCards() {
	fmt.Printf("deck before shuffling: %s\n", g.Deck.FormatForTerminal())
}

func (g *Game) DealPlayersIn() {
	for i := 0; i < playerCount; i++ {
		p := Player{Hand: g.Deck.Deal(2), Position: i + 1}
		g.Players = append(g.Players, &p)
	}
}

func (g *Game) ShowGameStatus() {
	fmt.Printf("Street: %s\n", g.StreetAsString())
	fmt.Printf("Community cards: %s\n", g.Community.FormatForTerminal())
	for _, p := range g.Players {
		if p != nil {
			fmt.Printf("Player %d: %s\n", p.Position, p.Hand.FormatForTerminal())
			if g.Street > 0 {
				ac := append(p.Hand, g.Community...)
				ph, err := ac.PokerHand()
				if err != nil {
					panic(err)
				}
				fmt.Printf("Player %d has %s\n", p.Position, ph.Description())
				fmt.Printf("Player %d Score: %d\n", p.Position, ph.Score)
			}
		}
	}
}

func (g *Game) DealFlop() {
	g.Community = g.Deck.Deal(3)
	g.Street = 1
}

func (g *Game) DealTurn() {
	g.Community = append(g.Community, g.Deck.Deal(1)...)
	g.Street = 2
}

func (g *Game) DealRiver() {
	g.Community = append(g.Community, g.Deck.Deal(1)...)
	g.Street = 3
}

func (g *Game) ShowWinner() {
	var winner *Player
	var winningHand *pokercore.PokerHand
	for _, p := range g.Players {
		if p != nil {
			ac := append(p.Hand, g.Community...)
			ph, err := ac.PokerHand()
			if err != nil {
				panic(err)
			}
			if winner == nil {
				winner = p
				winningHand = ph
			} else {
				if ph.Score > winningHand.Score {
					winner = p
					winningHand = ph
				}
			}
		}
	}
	fmt.Printf("Winner: Player %d with %s.\n", winner.Position, winningHand.Description())
}

func main() {
	g := NewGame()

	g.ShowGameStatus()
	g.DealPlayersIn()
	suspense()

	g.ShowGameStatus()
	g.DealFlop()
	suspense()

	g.ShowGameStatus()
	g.DealTurn()
	suspense()

	g.ShowGameStatus()
	g.DealRiver()
	suspense()

	g.ShowGameStatus()
	g.ShowWinner()

	fmt.Printf("What a strange game.  The only winning move is to bet really big.\n")
	fmt.Printf("Insert coin to play again.\n")
}
